'use strict';

$(function () {
    var expandProducts = $('.count_products__text');
    var expandDocByDate = $('.accounting__date');
    expandProducts.on('click', function (e) {
        var target = e.target;
        $(target).siblings('.accounting__products').slideToggle();
    });
    expandDocByDate.on('click', function (e) {
        var target = e.target;
        $(target).closest('.accounting__bydate').children('.accounting__bydocs').slideToggle();
        $(target).closest('.accounting__date').children('.arrow-toggle').toggleClass('down');
    });
});