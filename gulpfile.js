var gulp = require('gulp'),	
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
	livereload = require('gulp-livereload'),
	connect = require('gulp-connect'),
    babel = require('gulp-babel'),
    pug = require('gulp-pug');    

gulp.task('connect', function() {
	connect.server({
		root: 'dist',
		livereload: true
	})
})

gulp.task('css', function() {
    gulp.src('app/scss/styles.scss')
        .pipe(sass())
        .pipe(autoprefixer({
            browsers: ['> 0.1%'],
            cascade: false
        }))
		.pipe(gulp.dest("dist/css"))
		.pipe(connect.reload())	
});

gulp.task('js', function() {
    gulp.src('app/js/main.js')
		.pipe(babel({
			presets: ['es2015']
		}))
		.pipe(gulp.dest('dist/js'))
		.pipe(connect.reload())
});

gulp.task('html', function() {
    /* If you need, i left my sql query to get json from sqlite db
    SELECT d.date, dt.name, d.id as numberDoc, p.image, p.name, p.price, r.quantity, r.removed FROM rows r
    Inner join docs d on d.id = r.docid
    inner join docTypes dt on dt.id = d.typeid
    inner join products p on r.productId = p.id
    where p.removed is 0 and d.removed is 0 and dt.removed is 0 and r.removed is 0
    */
    var data = require('./data.json');
    gulp.src('app/index.pug')
        /* .pipe(data(file => require('./data.json'))) */
        .pipe(pug({ pretty:true, locals: data }))
        .pipe(gulp.dest('dist'))
});

gulp.task('reloadHTML', function() {
    gulp.src('dist/index.html')
		.pipe(connect.reload())
})

gulp.task('default', ['watch','css', 'html', 'js', 'connect']);

gulp.task('watch', function() {
    gulp.watch('app/scss/styles.scss', ['css']);
    gulp.watch('app/index.pug', ['html']);
	gulp.watch('dist/index.html', ['reloadHTML']);
	gulp.watch('app/js/main.js', ['js']);
});